package com.epam.online.rybka.task.other.entity;

/**
 * Cyprinus
 *
 * @author Andrii Rybka
 * @version 1.0
 */

public class Cyprinus extends Fish {

    /**
     * Constructor
     *
     * @param name  cyprinus name.
     * @param color cyprinus color.
     */

    public Cyprinus(String name, String color) {
        super(name, color);
    }
}
