package com.epam.online.rybka.task.other;

import java.util.ArrayList;
import java.util.List;

/**
 * Utilite class
 *
 * @author Andrii Rybka
 * @version 1.0
 */

public class Utilite<T> {
    private List<? super T> utilite = new ArrayList<>();

    /**
     * Add element to queue
     *
     * @param element item that should add
     */

    public void addElement(T element) {
        utilite.add(element);
    }

    /**
     * Get element by index
     *
     * @param index position of element
     * @return position in array
     */

    public T getElement(int index) {
        return (T) utilite.get(index);
    }

    /**
     * Get size
     *
     * @return size
     */

    public int getAge() {
        return utilite.size();
    }
}
