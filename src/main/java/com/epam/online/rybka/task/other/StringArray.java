package com.epam.online.rybka.task.other;

/**
 * StringArray class
 *
 * @author Andrii Rybka
 * @version 1.0
 */

public class StringArray {
    private String[] strings;
    private int length;
    private int size;

    public StringArray() {
        size = 2;
        length = 0;
        strings = new String[size];
    }

    public void resize() {
        size += size;
        String[] newStringArray = new String[size];
        for (int i = 0; i < strings.length; i++) {
            newStringArray[i] = strings[i];
        }
        strings = newStringArray;
    }

    /**
     * Add element to array.
     *
     * @param obj element
     */

    public void add(String obj) {
        if (length == size) {
            resize();
        }
        strings[length] = obj;
        length++;
    }

    /**
     * Get element from array.
     *
     * @param index index of element
     * @return string of array
     */

    public String get(int index) {
        return strings[index];
    }

}
