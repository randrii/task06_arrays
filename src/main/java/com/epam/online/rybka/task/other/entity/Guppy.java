package com.epam.online.rybka.task.other.entity;

import com.epam.online.rybka.task.other.entity.Fish;

/**
 * Guppy class
 *
 * @author Andrii Rybka
 * @version 1.0
 */

public class Guppy extends Fish {

    /**
     * Constror
     *
     * @param name  guppy's name.
     * @param color guppy's color.
     */

    public Guppy(String name, String color) {
        super(name, color);
    }
}
