package com.epam.online.rybka.task.other.entity;

import com.epam.online.rybka.task.other.entity.Fish;

/**
 * Silurus class
 *
 * @author Andrii Rybka
 * @version 1.0
 */
public class Silurus extends Fish {
    /**
     * Constructor
     *
     * @param name silurus name
     * @param color silurus color.
     */
    public Silurus(String name, String color) {
        super(name, color);
    }
}
