package com.epam.online.rybka.task.a;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * ArrayMain class
 * Array implementation
 *
 * @author Andrii Rybka
 * @version 1.0
 */

public class ArrayMain {
    public static Logger logger = LogManager.getLogger(ArrayMain.class);

    public static void main(String[] args) {
        Array arr = new Array();
        System.out.println();
        System.out.println(arr);

        System.out.println(arr.recurringElements());
        System.out.println(arr.uniqueElemnts());

        logger.info(arr.toString());
        logger.info("The elements present in both arrays" + arr.recurringElements());
        logger.info("The elements present in both arrays" + arr.uniqueElemnts());

    }

    @Override
    public String toString() {
        return "";
    }
}
