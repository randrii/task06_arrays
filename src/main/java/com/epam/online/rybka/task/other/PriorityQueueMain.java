package com.epam.online.rybka.task.other;

import com.epam.online.rybka.task.other.entity.Cyprinus;
import com.epam.online.rybka.task.other.entity.Fish;
import com.epam.online.rybka.task.other.entity.Guppy;
import com.epam.online.rybka.task.other.entity.Silurus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Queue;

/**
 * Main class
 * Priority Queue example
 *
 * @author Andrii Rybka
 * @version 1.0
 */

public class PriorityQueueMain {
    public static Logger logger = LogManager.getLogger(StringArrayMain.class);

    public static void main(String[] args) {
        StringArrayMain stringArrayMain = new StringArrayMain();
        Utilite<Fish> utilite = new Utilite<>();
        utilite.addElement(new Guppy("Guppy1", "white"));
        utilite.addElement(new Cyprinus("Cyprinus1", "grey"));
        utilite.addElement(new Silurus("Silurus1", "green"));
        for (int i = 0; i < utilite.getAge(); i++) {
            logger.info(utilite.getElement(i));
        }

        Queue<Integer> myPriorityQueue = new PriorityQueue<>((o1, o2) -> o2 - o1);
        myPriorityQueue.add(8);
        myPriorityQueue.add(11);
        myPriorityQueue.add(53);
        myPriorityQueue.add(44);
        myPriorityQueue.add(76);
        myPriorityQueue.add(9);
        myPriorityQueue.add(51);

        while (!myPriorityQueue.isEmpty()) {
            logger.info(myPriorityQueue.poll());
        }
    }
}
