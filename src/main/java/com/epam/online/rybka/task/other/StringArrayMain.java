package com.epam.online.rybka.task.other;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * StringArrayMain class
 * StringArray implementation
 *
 * @author Andrii Rybka
 * @version 1.0
 */

public class StringArrayMain {
    public static Logger logger = LogManager.getLogger(StringArrayMain.class);
    public static void main(String[] args) {
        StringArray str = new StringArray();
        List<String> list = new ArrayList<>(4);
        list.add("Bob");
        list.add("Lola");
        list.add("Bim");
        list.add("Ann");
        list.add("Job");
        logger.info(list);

        str.add("Olya");
        str.add("Bodya");
        str.add("Pavlo");
        str.add("Iryna");
        str.add("Nastya");
        logger.info(str);

    }

}
