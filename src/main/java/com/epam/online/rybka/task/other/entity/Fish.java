package com.epam.online.rybka.task.other.entity;

/**
 * Fish class
 *
 * @author Andrii Rybka
 * @version 1.0
 *
 */

public class Fish {

    /**
     * Fish's name and color
     */

    protected String name;
    protected String color;

    /**
     * Constructor.
     *
     * @param name  fish's name
     * @param color fish's color
     */

    public Fish(String name, String color) {
        this.name = name;
        this.color = color;
    }

    /**
     * to String method
     *
     * @return String of name and color
     */

    @Override
    public String toString() {
        return "name='" + name + '\n' +
                ", color='" + color + '\n';
    }
}
