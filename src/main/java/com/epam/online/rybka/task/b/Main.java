package com.epam.online.rybka.task.b;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Represents a main class.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-23-10
 */

public class Main {
    public static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        Array arr = new Array();
        System.out.println(arr);
        System.out.println(arr.removeRepeatedElements());

        logger.info(arr.removeRepeatedElements());
    }
}
