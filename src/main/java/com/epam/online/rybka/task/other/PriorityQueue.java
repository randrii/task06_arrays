package com.epam.online.rybka.task.other;

import java.util.*;

/**
 * PriorityQueue class.
 *
 * @author Andrii Rybka
 * @version 1.0
 */

public class PriorityQueue<T> implements Queue {
    List<T> queue;
    Comparator<T> comparator;

    PriorityQueue(Comparator<T> comparator) {
        queue = new ArrayList<>();
        this.comparator = comparator;
    }

    /**
     * Find head number in queue
     *
     * @return head number
     */

    private int findHeadNumber() {
        int headNumber = 0;
        for (int i = 0; i < size(); i++) {
            if (comparator.compare(queue.get(i), queue.get(headNumber)) > 0) {
                headNumber = i;
            }
        }
        return headNumber;
    }

    /**
     * Add element to queue
     *
     * @param o specific element
     * @return true if element is added
     */

    @Override
    public boolean add(Object o) {
        if (queue.add((T) o)) return true;
        else return false;
    }

    /**
     * Remove a specific object from queue
     *
     * @param o specific element
     * @return true if element is removed
     */

    @Override
    public boolean remove(Object o) {
        return queue.remove(o);
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    /**
     * Size method.
     *
     * @return size of queue.
     */

    @Override
    public int size() {
        return queue.size();
    }

    /**
     * Identifies the head element of queue
     *
     * @return calls if head exists, else null
     */

    @Override
    public Object peek() {
        return queue.get(findHeadNumber());
    }

    /**
     * Identifies head and then removes it
     *
     * @return calls if head exists, else null
     */

    @Override
    public Object poll() {
        if (size() == 0) {
            return null;
        }
        int headNumber = findHeadNumber();
        Object obj = queue.get(headNumber);
        queue.remove(headNumber);
        return obj;
    }

    @Override
    public Object element() {
        return null;
    }

    /**
     * Offer is required to insert a specific element to the given priority queue
     *
     * @param o specific element
     * @return call return true
     */

    @Override
    public boolean offer(Object o) {
        return true;
    }

    @Override
    public Object remove() {
        return null;
    }

    /**
     * Check if it is empty
     *
     * @return true if size is not null
     */

    @Override
    public boolean isEmpty() {
        if (size() == 0) return true;
        else return false;
    }

    /**
     * Returns true if priority queue contains element 'o'
     *
     * @param o specific element
     * @return true if priority queue contains element 'o'
     */

    @Override
    public boolean contains(Object o) {
        return false;
    }

    /**
     * Iterate over the queue element.
     *
     * @return calls iterator over the elements in queue
     */

    @Override
    public Iterator iterator() {
        return null;
    }

    /**
     * Returns array containing elements of PriorityQueue
     *
     * @return array containing all elements of PriorityQueue
     */

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    /**
     * Returns  array having elements of Priority Queue.
     *
     * @param a specific element
     * @return call array containing all elements
     */

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    /**
     * Clears all elements of Priority Queue
     */

    @Override
    public void clear() {

    }

}
