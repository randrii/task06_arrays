package com.epam.online.rybka.task.a;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Array class.
 *
 * Find the elements present in both arrays.
 * Find the elements present in one of the arrays.
 *
 * @author Andrii Rybka
 * @version 1.0
 */

public class Array {
    private List<Integer> list1;
    private List<Integer> list2;
    private List<Integer> list3;

    Random random = new Random();

    public Array() {
        list1 = new ArrayList<>();
        list2 = new ArrayList<>();
        list3 = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            list1.add(i);
        }
        for (int i = 2; i < 7; i++) {
            list2.add(i);
        }
        for (int i = 0; i < 7; i++) {
            list3.add(random.nextInt(5));
        }
    }

    /**
     * Find the elements present in both arrays.
     *
     * @return array of recurring elements.
     */

    public List<Integer> recurringElements() {
        List<Integer> result = new ArrayList<>();

        for (Integer i : list1) {
            for (Integer j : list2) {
                if (i.equals(j)) {
                    result.add(i);
                }
            }
        }
        return result;
    }

    /**
     * Merge two arrays.
     *
     * @return array.
     */

    public List<Integer> mergeTwoLists() {
        List<Integer> result = Stream.concat(list1.stream(), list2.stream()).collect(Collectors.toList());
        return result;
    }

    /**
     * Find the elements present in one of the arrays.
     *
     * @return arrays of unique elements.
     */

    public List<Integer> uniqueElemnts() {
        List<Integer> result = mergeTwoLists();
        for (int i = 0; i < result.size(); i++) {
            for (int j = 0; j < list1.size(); j++) {
                for (int k = 0; k < list2.size(); k++) {
                    if (result.get(i) == list1.get(j) && result.get(i) == list2.get(k)) {
                        result.remove(i);
                    }
                }
            }
        }
        return result;
    }

    /**
     * toString method
     *
     * @return array as string.
     */

    @Override
    public String toString() {
        return "list1: " + list1 + "\n" +
                "list2: " + list2 + "\n" +
                "list3: " + list3;
    }
}
