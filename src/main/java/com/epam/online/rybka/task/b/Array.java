package com.epam.online.rybka.task.b;

import java.util.ArrayList;
import java.util.List;

/**
 * Array1 class
 * Delete twice repeated item
 *
 * @author Andrii Rybka
 * @version 1.0
 */

public class Array {
    List<Integer> list;

    public Array() {
        list = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            list.add((int) (Math.random() * 10));
        }

    }

    /**
     * Find repeated items count
     *
     * @param element element of array
     * @return number of repetitions
     */

    private int isRepeatedElement(int element) {
        int counter = 0;
        for (Integer i : list) {
            if (i == element) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Remove twice repeated item
     *
     * @return array of integer
     */

    public List<Integer> removeRepeatedElements() {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (!(isRepeatedElement(list.get(i)) > 2)) {
                result.add(list.get(i));
            }
        }
        return result;
    }

    /**
     * Convert array to string.
     *
     * @return string of array
     */

    @Override
    public String toString() {
        return "list=" + list;
    }
}
