package com.epam.online.rybka.task.c;

import com.epam.online.rybka.task.other.StringArrayMain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Main class.
 * Search in an array of all series of identical
 * consecutive elements and remove all but one of them.
 *
 * @author Andrii Rybka
 * @version 1.0
 * @since 2019-26-11
 */

public class Main {
    public static Logger logger = LogManager.getLogger(StringArrayMain.class);

    public static void main(String[] args) {
        List<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(2);
        a.add(2);
        a.add(1);
        a.add(3);
        a.add(3);
        a.add(1);
        a.add(5);
        System.out.println(a);

        List<Integer> b = new ArrayList<>();
        boolean t = false;
        int temp = 0;
        int k = 1;
        for (int i = 1; i < a.size(); i++) {
            if (a.get(i) == a.get(i - 1)) {
                t = true;
                temp = a.get(i);
                k++;
            } else {
                t = false;
            }
            if (!t && k > 1) {
                for (int j = 0; j < k; j++) {
                    b.add(temp);
                }
                k = 1;
                a.remove(temp);
            }
        }
        if (t && k > 1) {
            for (int j = 0; j < k; j++) {
                b.add(temp);
            }
        }
        System.out.println(a);

        logger.info(a);
    }
}
