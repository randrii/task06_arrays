package task;

import java.util.Arrays;
import java.util.Random;

public class TaskA {
    private static int[] array1 = new int[10];
    private static int[] array2 = new int[array1.length];

    public static void start() {
        initArray();
        removeDuplicateElements(array1);
        removeDuplicateElements(array2);

        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
    }

    private static void initArray() {
        Random random = new Random();

        for (int i = 0; i < array2.length; i++) {
            array1[i] = random.nextInt(5);
            array2[i] = random.nextInt(5);
        }

        Arrays.sort(array1);
        Arrays.sort(array2);

        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
    }

    public static void removeDuplicateElements(int[] arr) {

        for (int i = 0; i < arr.length-1; i++) {
            if (arr[i] != arr[i + 1]) {
                arr[i] = arr[i];
            }
        }
    }

    public int[] getArray1() {
        return array1;
    }

    public int[] getArray2() {
        return array2;
    }
}
